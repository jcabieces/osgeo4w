::--------- Package settings --------
:: package name
set P=famsa
:: version
set V=1.6.2
:: package version
set B=2

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1

set HERE=%CD%

rd /s /q c:\install
:: ms extensions must be activated
mkdir c:\install\bin


wget --progress=bar:force https://github.com/refresh-bio/FAMSA/archive/v%V%.tar.gz -O %P%.tar.gz || goto :error
tar xzf %P%.tar.gz  || goto :error
cd FAMSA-%V%  || goto :error

:: apply patch
patch -p0 < %HERE%/patches/patch_src-msa.cpp || goto :error

:: msbuild /nologo /t:Build  /property:Configuration=Release FAMSA.sln  || goto :error
devenv /Build "Release|x64" FAMSA.sln || goto :error

move x64\Release\%P%.exe c:\install\bin

:: binary archive
c:\cygwin64\bin\tar.exe  -C c:\install -cjvf %PKG_BIN% bin || goto :error

:: source archive
c:\cygwin64\bin\tar.exe  -C %HERE% --transform 's,^,osgeo4w/,' -cvjf %PKG_SRC% package.cmd setup.hint || goto :error

::--------- Installation
scp %PKG_BIN% %PKG_SRC% %R% || goto :error
cd %HERE%
scp setup.hint %R% || goto :error
goto :EOF

:error
echo Build failed
exit /b 1
