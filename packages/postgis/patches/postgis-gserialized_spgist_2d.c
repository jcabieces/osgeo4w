--- "postgis\\gserialized_spgist_2d.c.orig"
+++ "postgis\\gserialized_spgist_2d.c"
@@ -73,7 +73,7 @@
 #include <postgres.h>
 #include <utils/builtins.h>    /* For float manipulation */
 #include "access/spgist.h"     /* For SP-GiST */
-#include "catalog/pg_type_d.h" /* For VOIDOID */
+#include "catalog/pg_type.h" /* For VOIDOID */
 
 #include "../postgis_config.h"
 
@@ -287,14 +287,16 @@ overAbove4D(RectBox *rect_box, BOX2DF *query)
 
 PG_FUNCTION_INFO_V1(gserialized_spgist_config_2d);
 
-PGDLLEXPORT Datum gserialized_spgist_config_2d(PG_FUNCTION_ARGS)
+ Datum gserialized_spgist_config_2d(PG_FUNCTION_ARGS)
 {
 	spgConfigOut *cfg = (spgConfigOut *)PG_GETARG_POINTER(1);
 
 	Oid boxoid = postgis_oid_fcinfo(fcinfo, BOX2DFOID);
 	cfg->prefixType = boxoid;
 	cfg->labelType = VOIDOID; /* We don't need node labels. */
+	#if POSTGIS_PGSQL_VERSION >= 110
 	cfg->leafType = boxoid;
+	#endif
 	cfg->canReturnData = false;
 	cfg->longValuesOK = false;
 
@@ -307,7 +309,7 @@ PGDLLEXPORT Datum gserialized_spgist_config_2d(PG_FUNCTION_ARGS)
 
 PG_FUNCTION_INFO_V1(gserialized_spgist_choose_2d);
 
-PGDLLEXPORT Datum gserialized_spgist_choose_2d(PG_FUNCTION_ARGS)
+ Datum gserialized_spgist_choose_2d(PG_FUNCTION_ARGS)
 {
 	spgChooseIn *in = (spgChooseIn *)PG_GETARG_POINTER(0);
 	spgChooseOut *out = (spgChooseOut *)PG_GETARG_POINTER(1);
@@ -331,7 +333,7 @@ PGDLLEXPORT Datum gserialized_spgist_choose_2d(PG_FUNCTION_ARGS)
  */
 PG_FUNCTION_INFO_V1(gserialized_spgist_picksplit_2d);
 
-PGDLLEXPORT Datum gserialized_spgist_picksplit_2d(PG_FUNCTION_ARGS)
+ Datum gserialized_spgist_picksplit_2d(PG_FUNCTION_ARGS)
 {
 	spgPickSplitIn *in = (spgPickSplitIn *)PG_GETARG_POINTER(0);
 	spgPickSplitOut *out = (spgPickSplitOut *)PG_GETARG_POINTER(1);
@@ -402,7 +404,7 @@ PGDLLEXPORT Datum gserialized_spgist_picksplit_2d(PG_FUNCTION_ARGS)
  */
 PG_FUNCTION_INFO_V1(gserialized_spgist_inner_consistent_2d);
 
-PGDLLEXPORT Datum gserialized_spgist_inner_consistent_2d(PG_FUNCTION_ARGS)
+ Datum gserialized_spgist_inner_consistent_2d(PG_FUNCTION_ARGS)
 {
 	spgInnerConsistentIn *in = (spgInnerConsistentIn *)PG_GETARG_POINTER(0);
 	spgInnerConsistentOut *out = (spgInnerConsistentOut *)PG_GETARG_POINTER(1);
@@ -552,7 +554,7 @@ PGDLLEXPORT Datum gserialized_spgist_inner_consistent_2d(PG_FUNCTION_ARGS)
  */
 PG_FUNCTION_INFO_V1(gserialized_spgist_leaf_consistent_2d);
 
-PGDLLEXPORT Datum gserialized_spgist_leaf_consistent_2d(PG_FUNCTION_ARGS)
+ Datum gserialized_spgist_leaf_consistent_2d(PG_FUNCTION_ARGS)
 {
 	spgLeafConsistentIn *in = (spgLeafConsistentIn *)PG_GETARG_POINTER(0);
 	spgLeafConsistentOut *out = (spgLeafConsistentOut *)PG_GETARG_POINTER(1);
@@ -659,7 +661,7 @@ PGDLLEXPORT Datum gserialized_spgist_leaf_consistent_2d(PG_FUNCTION_ARGS)
 
 PG_FUNCTION_INFO_V1(gserialized_spgist_compress_2d);
 
-PGDLLEXPORT Datum gserialized_spgist_compress_2d(PG_FUNCTION_ARGS)
+ Datum gserialized_spgist_compress_2d(PG_FUNCTION_ARGS)
 {
 	Datum gsdatum = PG_GETARG_DATUM(0);
 	BOX2DF *bbox_out = palloc(sizeof(BOX2DF));