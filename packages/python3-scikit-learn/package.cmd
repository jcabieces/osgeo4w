::--------- Package settings --------
:: package name
set P=python3-scikit-learn
:: version
set V=0.23.2
:: package version
set B=1
:: name
set N=scikit-learn

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
call %OSGEO4W_ROOT%\bin\py3_env.bat
set PATH=%OSGEO4W_ROOT%\bin;%PATH%
set SITEPACKAGES=apps/Python37/Lib/site-packages

python3 -m pip install %N%==%V% || goto :error

:: archive (add joblib and threadpool too)
c:\cygwin64\bin\tar -C %OSGEO4W_ROOT% -cvjf %PKG_BIN% ^
	%SITEPACKAGES%/scikit_learn-%V%.dist-info ^
	%SITEPACKAGES%/sklearn ^
	%SITEPACKAGES%/threadpoolctl.py ^
	%SITEPACKAGES%/threadpoolctl-2.1.0.dist-info ^
	%SITEPACKAGES%/joblib-0.17.0.dist-info ^
	%SITEPACKAGES%/joblib || goto: error

::--------- Installation
scp %PKG_BIN% %R%
cd %HERE%
scp setup.hint %R%
