#!/bin/bash

if [ $# -lt 2 ]; then
    echo "Usage: packages.sh <delivery_env> <package1> <package2> ..."
    exit 1
fi

set -e

DELIVERY_ENV=$1
shift

SCRIPT_DIR=$(dirname "$(readlink -f "$0")")

# Order package according to their dependency: If A depends on B, build B first, then A

packages=($*)
ordered_packages=()

while (( ${#packages[@]} > 0 ))
do
    package=${packages[0]}

    others=$(printf "|%s" "${packages[@]:1}")
    others=${others:1}

    # Does package have dependencies to be built first?
    if (( ${#packages[@]} > 1 )) && grep -q -E "requires.*(${others})" $SCRIPT_DIR/../packages/${package}/setup.hint ; then
	packages=(${packages[@]:1} $package)
    else
	ordered_packages=(${ordered_packages[@]} $package)
	packages=(${packages[@]:1})
    fi
done

echo "Build packages order:${ordered_packages[@]}"

for PACKAGE in ${ordered_packages[@]};
do
    echo "Build package $PACKAGE ..."
    cd $SCRIPT_DIR/../packages/${PACKAGE}
    if [ -x package.sh ]; then /bin/bash ./package.sh ${DELIVERY_ENV}; else cmd /c "package.cmd ${DELIVERY_ENV}"; fi

    # update setup ini file for each package so next builded package will use the new version one
    cd $SCRIPT_DIR/..
    scp scripts/genini scripts/gen_setup_ini.sh ci@hekla.oslandia.net:~/
    ssh ci@hekla.oslandia.net ./gen_setup_ini.sh ${DELIVERY_ENV}
done



