# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "win10-vs2017"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = true
  
    # Customize the amount of memory on the VM:
    vb.memory = "4096"

    # to configure VM parameters https://docs.oracle.com/en/virtualization/virtualbox/6.0/user/vboxmanage-modifyvm.html

    # Customize the amount of video memory on the VM
    vb.customize ["modifyvm", :id, "--vram", "64"]

    # set graphics controller to vboxsvga
    vb.customize ["modifyvm", :id, "--graphicscontroller", "vboxsvga"]

    # disable remote desktop
    vb.customize ["modifyvm", :id, "--vrde", "off"]

    # to enable mouse automatic integration
    vb.customize ["modifyvm", :id, "--usb", "on"]
    vb.customize ["modifyvm", :id, "--mouse", "usbtablet"]
  end

  # Enable ssh guest connexion (for Gitlab-runner)
  config.ssh.forward_agent = true

  # TODO do we need to reboot or not, or could we do it only once at the end
  #  choco feature enable -name=exitOnRebootDetected

  # VisualStudio Component list
  # https://docs.microsoft.com/en-us/visualstudio/install/workload-component-id-vs-professional?view=vs-2017

  # Try to install less component : without optionnal for instance, or just with the following
  # Microsoft.VisualStudio.Component.VC.140
  # Microsoft.VisualStudio.Component.Windows10SDK.18362


  # Install openssh server : https://cygwin.fandom.com/wiki/Sshd
  
    $script = <<-SCRIPT
  echo "Provision VM..."
  [Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
  iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
  chocolatey feature enable -n allowGlobalConfirmation
  choco install visualstudio2017community --timeout 5400 --package-parameters "--add Microsoft.VisualStudio.Workload.NativeDesktop --includeRecommended --includeOptional --passive"

  # We need to install dbghelp.dll and symsrv.dll in the right place, I failed to find in which part of VS2017 installer this dll lives
  # so I copied it from a working installation
  New-Item -ItemType Directory -Force -Path "C:/Program Files (x86)/Microsoft Visual Studio 14.0/Common7/IDE/Remote Debugger/x64"
  cp C:/vagrant/dbghelp.dll "C:/Program Files (x86)/Microsoft Visual Studio 14.0/Common7/IDE/Remote Debugger/x64/"
  cp C:/vagrant/symsrv.dll "C:/Program Files (x86)/Microsoft Visual Studio 14.0/Common7/IDE/Remote Debugger/x64/"

  choco install dependencywalker
  choco install cmake --version 3.17.0
  choco install notepadplusplus

  echo "Downloading Cygwin..." 
  Invoke-WebRequest -OutFile c:/setup-x86_64.exe https://cygwin.com/setup-x86_64.exe

  echo "Installing Cygwin..."
  # PostGIS package needs patch package
  c:/setup-x86_64.exe -qnNdO -R C:/cygwin64 -s http://cygwin.mirror.constant.com -l C:/temp/cygwin -P "bison,flex,git,poppler,doxygen,unzip,patch,wget" | Out-Null

  echo "Downloading OSGEO4W..." 
  Invoke-WebRequest -OutFile c:/Users/Vagrant/osgeo4w-setup-x86_64.exe http://download.osgeo.org/osgeo4w/osgeo4w-setup-x86_64.exe
  echo "Instaling OSGEO4W..." 
  C:/Users/vagrant/osgeo4w-setup-x86_64.exe -g -q -A -R C:/OSGeo4W64 -a x86_64 -k -O -s http://osgeo4w-oslandia.com/mirror -P setup -P shell -P setuptools | Out-Null

  echo "Downloading Ninja..." 
  Invoke-WebRequest -OutFile c:/ninja.zip https://github.com/ninja-build/ninja/releases/download/v1.9.0/ninja-win.zip
  echo "Installing Ninja..." 
  C:/cygwin64/bin/unzip -o c:/ninja.zip -d C:/OSGeo4W64/bin 

  # Configure SSH server (ssh-hist-config has to be executed in cygwin)
  # echo "Configure SSH server... (for Gitlab-runner)"
  # ssh-host-config --yes -N ntsec
  # net start

  SCRIPT
    
  config.vm.provision "shell", inline: $script
    
end
